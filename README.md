# Что нужно сделать

## 1 сервер
**Что нужно сделать**

- Зарегистрируйте аккаунт в AWS, активируйте [бесплатный доступ](https://aws.amazon.com/ru/free/activate-your-free-tier-account/) и получите 300 долларов на счёт. 
- Установите [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) любой версии и [сконфигурируйте его](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) для управления аккаунтом и ресурсами. 
- Перейдите в [настройки](https://console.aws.amazon.com/ec2/), на панели навигации выберите Key Pairs и [загрузите свой SSH-ключ](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html), его имя будет использоваться в Terraform-скрипте.
- Скачайте и установите [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli).
- Установите [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
- Возьмите код из репозитория `git clone https://gitlab.com/fominraos/meet-ansible.git`
- В файле `OneServer/TerraformOneServer/main.tf` в блоке `resource "aws_instance" "my_webserver"` для параметра `key_name` добавьте название ключа из пункта 3.
- Перейдите в папку OneServer/TerraformOneServer и инициализируйте ресурсы при помощи Terraform `terraform init`.
- Разверните инфраструктуру при помощи команды `terraform apply`.
- Добавьте IP-адрес полученного сервера в инвентори-файл `hosts`.
- Запустите плейбук `ansible-playbook -b reactjs.yaml -vv`.
- После запуска плейбука убедитесь, что приложение ReactJS доступно из браузера.
- Выполнить `terraform destoy` для удаления ресурсов

----

## 2 сервера
**Что нужно сделать**

- Разверните инфраструктуру с балансировкой нагрузки и двумя серверами при помощи Terraform из папки TwoServers, узнайте DNS-имя балансировщика.
- Узнайте их IP при помощи AWS CLI или в консоли AWS в регионе us-east-1.
- Добавьте их в инвентори-файл `hosts`.
- Запустите плейбук Ansible и попробуйте перейти в браузере по DNS-имени балансировщика.

